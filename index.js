#!/usr/bin/env node

require('dotenv').config()

const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const session = require('express-session')
const fileUpload = require('express-fileupload')
const browserify = require('browserify')
const i18n = require('i18n')
const mysql = require('mysql')
const sass = require('sass')
const MySQLStore = require('express-mysql-session')(session);
const { form_flatten } = require('./utils')
const app = express()

const mysqlConnection = mysql.createConnection({
  host: process.env.DB_HOST || 'localhost',
  port: process.env.DB_PORT || 3306,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  charset : 'utf8mb4',
  multipleStatements: true,
  stringifyObjects: true
})
const sessionStore = new MySQLStore({}, mysqlConnection)
global.models = require('./models')(mysqlConnection)

i18n.configure({
  locales: ['fr', 'en', 'es'],
  queryParameter: 'lang',
  indent: '  ',
  directory: path.join(__dirname, 'locales'),
  defaultLocales: 'en',
  autoReload: true,
  syncFiles: true
})

app.set('view engine', 'pug')
app.set('x-powered-by', false)
app.set('trust proxy', 'loopback')

const PORT = process.env.PORT || 3000
app.listen(PORT, () => {
  console.log(`Hammerfest Zero web server listening on port ${PORT}.`)
})

app.use(bodyParser.urlencoded({extended: true}))
app.use(express.static(path.join(__dirname, 'public')))
app.use(session({
  key: 'sid',
  secret: process.env.COOKIE_SECRET,
  store: sessionStore,
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 1000 * 3600 * 24 * 365 * 10
  }
}))
app.use(fileUpload())
app.use(i18n.init)
app.use(function(req, res, next) {
  req.locals = res.locals
  req.locals.session = req.session
  req.locals.form = {errors: {},
                     values: req.body ? form_flatten(req.body) : {},
                     "default": {}}
  next()
})

app.get('/css/style.css', (req, res) => {
  // TODO: cache this
  sass.render({
    "outputStyle": "compressed",
    "file": "./styles/style.scss"
  }, (e, r) => {
    res.contentType('text/css')
    res.send(r.css)
  })
})

app.get('/js/editor.dist.js', (req, res) => {
  // TODO: cache and minify this
  res.contentType('text/javascript')
  const b = browserify(path.join(__dirname, 'public', 'js', 'editor.js'))
  b.bundle((err, buff) => res.send(buff))
})

app.get('/', (req, res) => res.render('index'))
app.get('/play/manage', (req, res) => res.send('gid=1&key=8571340b8d5711249d84dc1e1e8bce2&families=0,1,2,3,4,10,11,12,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030,5000,5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5012,5014'))

app.use('/adventures', require('./routes/adventures'))
app.use('/', require('./routes/users'))

app.use((req, res) => {
  res.statusCode = 404
  res.render('404')
})
