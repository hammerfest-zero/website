# Hammerfest Zero Website

This is the website of the project (currently available at [https://hf-zero.rainbowda.sh](https://hf-zero.rainbowda.sh)).

## Installation

This website requires `node.js`, `node-gyp`, `npm`, `mariadb` and the `mtypes` compiler.

```sh
echo "CREATE DATABASE hammerfest_zero;
CREATE USER 'hammerfest-zero'@'localhost' IDENTIFIED BY 'secret_pass';
GRANT ALL PRIVILEGES ON hammerfest_zero.* TO 'hammerfest-zero'@'localhost';
FLUSH PRIVILEGES" | mariadb -u root -p

echo "DB_HOST=localhost
DB_NAME=hammerfest_zero
DB_USER=hammerfest-zero
DB_PASS=secret_pass
DB_PORT=3306
COOKIE_SECRET=secret_cookie
PORT=3000" > .env
chmod 600 .env
npm run migrate -- up

./swf/compile.sh
npm ci
```

```sh
npm start
```

**⚠ Warning**: You should either edit those commands or remove them from your shell history afterwards since they contain non-encrypted passwords.
