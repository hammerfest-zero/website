function flatten(obj, prefix = '') {
  const out = {}
  for (let [k, v] of Object.entries(obj)) {
    if (prefix !== '')
      k = prefix + '[' + k + ']'
    if (v !== null
        && typeof v === 'object')
      Object.assign(out, flatten(v, k))
    else
      out[k] = v
  }
  return out
}

exports.form_flatten = flatten
