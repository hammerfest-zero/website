const test = require('ava')
const { form_flatten } = require('../utils')

test('empty', t => {
  t.deepEqual(form_flatten({}), {})
})

test('simple types', t => {
  t.deepEqual(form_flatten({boolean: true}), {boolean: true})
  t.deepEqual(form_flatten({boolean: false}), {boolean: false})
  t.deepEqual(form_flatten({number: 42}), {number: 42})
  t.deepEqual(form_flatten({"null": null}), {"null": null})
  t.deepEqual(form_flatten({undef: undefined}), {undef: undefined})
  t.deepEqual(form_flatten({string: "foobar"}), {string: "foobar"})
})

test('simple array', t => {
  t.deepEqual(form_flatten({array: [1, 2, 3]}), {"array[0]": 1,
                                                 "array[1]": 2,
                                                 "array[2]": 3})
})

test('simple object', t => {
  t.deepEqual(form_flatten({obj: {a: 1, b: 2, c: 3}}), {"obj[a]": 1,
                                                        "obj[b]": 2,
                                                        "obj[c]": 3})
})

test('mixed_array_and_objects', t => {
  const orig = {
    obj_array: [
      {a: 1},
      {a: 2},
      {a: 3,
       b: true}
    ]
  }
  const flatten = {
    "obj_array[0][a]": 1,
    "obj_array[1][a]": 2,
    "obj_array[2][a]": 3,
    "obj_array[2][b]": true
  }
  t.deepEqual(form_flatten(orig), flatten)
})
