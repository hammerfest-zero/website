'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  db.createTable('user_preferences', {
    user_id: {
      type: 'int',
      notNull: true,
      foreignKey: {
        name: 'user_preferences_users_id_fk',
        table: 'users',
        mapping: 'id',
        rules: {
          onDelete: 'cascade',
          onUpdate: 'cascade'
        }
      }
    },
    display_name: {
      type: 'string',
      notNull: true,
      length: 64,
    },
    profile_private: {
      type: 'boolean',
      notNull: true,
      defaultValue: true
    },
    game_preferences: {
      type: 'json',
      notNull: true
    },
    modified_at: {
      type: 'timestamp',
      defaultValue: 'CURRENT_TIMESTAMP',
      onUpdate: 'CURRENT_TIMESTAMP'
    }
  }, function() {
    db.all('SELECT * FROM `users`', function(err, rows, fields) {
      if (err || rows.length < 1)
        callback(err, rows, fields)
      else {
        let query = 'INSERT INTO `user_preferences` (`user_id`, `display_name`, `game_preferences`) VALUES'
            + ' (?, ?, "{}"),'.repeat(rows.length).slice(0, -1)
        let parts = rows.flatMap(row => [row.id, row.name])
        db.runSql(query, parts, callback)
      }
    });
  });
};

exports.down = function(db) {
  return db.dropTable('user_preferences');
};

exports._meta = {
  "version": 1
};
