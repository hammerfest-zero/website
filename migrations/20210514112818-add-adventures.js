'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('adventures', {
    id: {
      type: 'int',
      notNull: true,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: 'int',
      notNull: true,
      foreignKey: {
        name: 'adventures_users_id_fk',
        table: 'users',
        mapping: 'id',
        rules: {
          onDelete: 'cascade',
          onUpdate: 'cascade'
        }
      }
    },
    group_id: {
      type: 'int',
      notNull: true,
      foreignKey: {
        name: 'adventures_groups_id_fk',
        table: 'groups',
        mapping: 'id',
        rules: {
          onDelete: 'cascade',
          onUpdate: 'cascade'
        }
      }
    },
    permissions_id: {
      type: 'int',
      notNull: true,
      foreignKey: {
        name: 'adventures_permissions_id_fk',
        table: 'permissions',
        mapping: 'id',
        rules: {
          onDelete: 'restrict',
          onUpdate: 'cascade'
        }
      }
    },
    forked_from_id: {
      type: 'int',
      notNull: false,
      defaultValue: null,
      foreignKey: {
        name: 'adventures_adventures_id_fk',
        table: 'adventures',
        mapping: 'id',
        rules: {
          onDelete: 'set null',
          onUpdate: 'cascade'
        }
      }
    },
    version: {
      type: 'string',
      notNull: true,
      defaultValue: '0.0.0'
    },
    body: {
      type: 'json',
      notNull: true
    },
    created_at: {
      type: 'timestamp',
      defaultValue: 'CURRENT_TIMESTAMP'
    },
    modified_at: {
      type: 'timestamp',
      defaultValue: 'CURRENT_TIMESTAMP',
      onUpdate: 'CURRENT_TIMESTAMP'
    }
  });
};

exports.down = function(db) {
  return db.dropTable('adventures');
};

exports._meta = {
  "version": 1
};
