'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  db.createTable('groups', {
    id: {
      type: 'int',
      notNull: true,
      primaryKey: true,
      autoIncrement: true
    },
    owner_id: {
      type: 'int',
      notNull: true,
      foreignKey: {
        name: 'user_groups_owner_id_fk',
        table: 'users',
        mapping: 'id',
        rules: {
          onUpdate: 'restrict',
          onDelete: 'cascade'
        }
      }
    },
    name: {
      type: 'string',
      unique: true,
      notNull: true,
      length: 80
    },
    created_at: {
      type: 'timestamp',
      defaultValue: 'CURRENT_TIMESTAMP'
    },
    modified_at: {
      type: 'timestamp',
      defaultValue: 'CURRENT_TIMESTAMP',
      onUpdate: 'CURENT_TIMESTAMP'
    }
  }, function() {
    db.createTable('users_groups', {
      user_id: {
        type: 'int',
        notNull: true,
        foreignKey: {
          name: 'user_groups_users_id_fk',
          table: 'users',
          mapping: 'id',
          rules: {
            onUpdate: 'restrict',
            onDelete: 'cascade'
          }
        }
      },
      group_id: {
        type: 'int',
        notNull: true,
        foreignKey: {
          name: 'user_groups_groups_id_fk',
          table: 'groups',
          mapping: 'id',
          rules: {
            onUpdate: 'restrict',
            onDelete: 'cascade'
          }
        }
      }
    }, callback)
  });
};

exports.down = function(db, callback) {
  db.dropTable('users_groups', function() {
    db.dropTable('groups', callback)
  })
};

exports._meta = {
  "version": 1
};
