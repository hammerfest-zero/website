'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('permissions', {
    id: {
      type: 'int',
      notNull: true,
      primaryKey: true,
      autoIncrement: true
    },
    group_read: {
      type: 'boolean',
      notNull: true,
      defaultValue: false
    },
    group_write: {
      type: 'boolean',
      notNull: true,
      defaultValue: false
    },
    group_fork: {
      type: 'boolean',
      notNull: true,
      defaultValue: false
    },
    other_read: {
      type: 'boolean',
      notNull: true,
      defaultValue: false
    },
    other_fork: {
      type: 'boolean',
      notNull: true,
      defaultValue: false
    },
    created_at: {
      type: 'timestamp',
      defaultValue: 'CURRENT_TIMESTAMP'
    },
    modified_at: {
      type: 'timestamp',
      defaultValue: 'CURRENT_TIMESTAMP',
      onUpdate: 'CURRENT_TIMESTAMP'
    }
  });
};

exports.down = function(db) {
  return db.dropTable('permissions');
};

exports._meta = {
  "version": 1
};
