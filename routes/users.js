const express = require('express')
const argon2 = require('argon2')
const checks = require('../checks')
const { form_flatten } = require('../utils')
const router = express.Router()

router.get('/register', checks.needNoAuth, (req, res) => res.render('register'))

router.post('/register', checks.needNoAuth, async (req, res) => {
  req.body.profile_private = req.body.profile_private === "on"
  const errors = res.locals.form.errors
  if (req.body.username !== undefined)
    req.body.username = String(req.body.username).trim()
  if (req.body.username === undefined
      || req.body.username === '')
    errors.username = "You must provide a username"
  if (req.body.display_name !== undefined)
    req.body.display_name = String(req.body.display_name).trim()
  if (req.body.display_name === undefined
      || req.body.display_name === '')
    errors.display_name = "You must provide a display name"
  if (req.body.password === undefined
      || req.body.password.length < 12)
    errors.password = "Your password must be at least 12 characters long"
  if (req.body.password !== req.body.password2)
    errors.password2 = "The passwords does not match"
  if (Object.keys(errors).length > 0) {
    res.statusCode = 400
    res.render('register')
  } else {
    try {
      await models.User.create(req.body)
      const [ e ] = await doLogin(req, req.body.username, req.body.password)
      if (e && e.code === 500) {
        res.statusCode = e.code
        errors.unknown = e.message
        res.render('register')
      } else {
        res.statusCode = 201
        res.render('index')
      }
    } catch(e) {
      if (e === "Username already in use.") {
        errors.username = e
        res.statusCode = 400
        res.render('register')
      } else {
        errors.unknown = "An unknown error has occured. Here are some logs if you want:" + e
        res.statusCode = 500
        res.render('register')
      }
    }
  }
})

async function doLogin(req, username, password) {
  try {
    const [ row ] = await models.User.getByName(username)
    if (row === undefined
        || await argon2.verify(row.password, password) !== true) {
      return [{code: 400,
               message: "Username or password invalid."}]
    } else {
      delete row.password
      req.session.is_logged = true
      req.session.user = row
      await models.Adventure.sessionToDb(req.session)
      return [undefined]
    }
  } catch(e) {
    return [{code: 500,
             message: "An unknown error has occured. Here are some logs if you want:" + e}]
  }
}

router.get('/login', checks.needNoAuth, (req, res) => res.render('login'))

router.post('/login', checks.needNoAuth, async (req, res) => {
  req.body.profile_private = req.body.profile_private === "on"
  const errors = res.locals.form.errors
  if (req.body.username !== undefined)
    req.body.username = String(req.body.username).trim()
  if (req.body.username === undefined
      || req.body.username === '')
    errors.username = "You must provide a username"
  if (Object.keys(errors).length > 0) {
    res.statusCode = 400
    res.render('login')
  } else {
    const [e] = await doLogin(req, req.body.username, req.body.password)
    if (e) {
      res.statusCode = e.code
      errors.unknown = e.message
      res.render('login')
    } else {
      res.render('index')
    }
  }
})

router.post('/logout', (req, res) => {
  req.session.is_logged = false
  delete req.session.user
  res.render('index')
})

router.get('/users/:name', async (req, res) => {
  try {
    const [ user ] = await models.User.getByName(req.params.name)
    if (user === undefined
        || (user.profile_private === true
            && (req.session.is_logged !== true
                || (req.session.user.id && user.id !== req.session.user.id)))) {
      res.statusCode = 404
      res.locals.error = "This user doesn't exist or their profil is private."
    } else {
      res.locals.user = user
    }
  } catch (e) {
    res.statusCode = 500
    res.locals.error = "An unknown error has occured. Here are some logs if you want:" + e
  }
  res.render('user_profile')
})

router.get('/user/edit', checks.needAuth, (req, res) => {
  res.locals.form.default = form_flatten(req.session.user)
  res.render('user_edit')
})

router.post('/user/edit', checks.needAuth, async (req, res, next) => {
  const errors = res.locals.form.errors
  /* Warning-TODO: would be erased if missing parameter (in the future API) */
  req.body.profile_private = req.body.profile_private === "on"
  if (req.body.display_name !== undefined)
    req.body.display_name = String(req.body.display_name).trim()
  if (req.body.display_name === '')
    errors.display_name = "Your display name cannot be empty"
  if ((req.body.password === undefined
       || req.body.password === '') &&
      (req.body.password2 === undefined
       || req.body.password2 === '')) {
    req.body.password = null
    delete req.body.password2
  } else {
    if (req.body.password.length < 12)
      errors.password = "Your password must be at least 12 characters long"
    if (req.body.password !== req.body.password2)
      errors.password2 = "The passwords does not match"
  }
  ["sound", "music", "detail", "shake", "use_ruffle"].forEach(opt => {
    req.body.game_preferences[opt] = req.body.game_preferences[opt] === "on"
  })
  req.body.game_preferences.volume = parseInt(req.body.game_preferences.volume)
  req.body.game_preferences.zoom = parseInt(req.body.game_preferences.zoom)
  if (Object.keys(errors).length > 0) {
    res.statusCode = 400
    res.locals.form.values = {}
    req.method = 'GET'
    router.handle(req, res, next)
  } else {
    try {
      await models.User.updateById(req.session.user.id, req.body)
      req.session.user.game_preferences = req.body.game_preferences
      if (req.body.display_name)
        req.session.user.display_name = req.body.display_name
      req.session.user.profile_private = req.body.profile_private
      req.method = 'GET'
      router.handle(req, res, next)
    } catch(e) {
      errors.unknown = "An unknown error has occured. Here are some logs if you want:" + e
      res.statusCode = 500
      res.locals.form.values = {}
      req.method = 'GET'
      router.handle(req, res, next)
    }
  }
})

module.exports = router
