const path = require('path')
const qs = require('querystring')
const express = require('express')
const Adventure = require('adventure')
const Level = require('level')
const router = express.Router()

const modes = {
  "adventure": ["solo", "multicoop", "bossrush"],
  "tutorial": ["tutorial"],
  "timeattack": ["timeattack", "multitime"],
  "soccer": ["soccer"]
}

const modes_options = {
  "solo": ["mirror", "nightmare", "ninja", "bombexpert", "boost"],
  "tutorial": [],
  "timeattack": [],
  "multicoop": ["mirrormulti", "nightmaremulti", "lifesharing", "bombexpert", "bombcontrol", "boost"],
  "multitime": [],
  "soccer": ["kickcontrol", "soccerbomb"],
  "bossrush": [],
  "editor": ["mirror", "nightmare", "ninja", "bombexpert", "boost"]
}

router.get('/', async (req, res) => {
  res.render("adventures/index", {adventures: await models.Adventure.getAll(req.session),
                                  allModes: modes})
})

router.get('/new', (req, res) => {
  res.render('adventures/new', {modes: modes,
                                allOptions: modes_options})
})

router.post('/new', async (req, res) => {
  if (!req.session.advs)
    req.session.advs = {}
  //TODO: validate inputs
  //TODO: manage modes and options
  const adv = new Adventure(req.body.type)
  adv.name.default = req.body.name
  adv.description.default = req.body.description
  adv.game_modes = new Adventure.GameModes(true)
  adv.game_options = new Adventure.GameOptions(true)
  adv.game.lives = parseInt(req.body.lives)
  adv.game.bombs = parseInt(req.body.bombs)
  Array.from(Array(req.body.dims)).forEach(async (_, i) => adv.levels.sets["file" + i] = (await (new Level()).toHF0()))
  const newAdvSlug = await models.Adventure.create(req.session, adv)
  res.redirect('/adventures/' + newAdvSlug)
})

router.get('/upload', (req, res) => res.render('adventures/upload'))

router.post('/upload', async (req, res) => {
  try {
    const adv = Adventure.fromJSON(JSON.parse(req.files.adv.data.toString()))
    const slug = await models.Adventure.create(req.session, adv)
    res.redirect('/adventures/' + slug)
  } catch (e) {
    res.status(400).render('adventures/upload', {'errors': ['Invalid adventure file']})
  }
})

router.get('/:adventure', async (req, res) => {
  const adv = await models.Adventure.getBySlug(req.session, req.params.adventure)
  if (adv === null) {
    return res.status(404).render('404')
  }
  res.render("adventures/play_form", {adventure: adv,
                                      modes: modes[adv.adventure.type].filter(m => adv.adventure.game_modes[m] === true),
                                      allOptions: modes_options})
})

router.post('/:adventure/delete', async (req, res) => {
  //TODO: error handling
  if (await models.Adventure.deleteBySlug(req.session, req.params.adventure)) {
    res.redirect('/adventures')
  } else {
    res.redirect('/adventures/' + req.params.adventure)
  }
})

router.get('/:adventure/:mode', async (req, res) => {
  res.locals.mode = req.params.mode
  res.locals.options = (req.query.options || []).join(',')
  if (req.session.user && req.session.user.game_preferences) {
    res.locals.game_preferences = {}
    for (const [name, val] of Object.entries(req.session.user.game_preferences)) {
      res.locals.game_preferences[name] = typeof val === 'boolean' ? Number(val) : val
    }
  } else {
    res.locals.game_preferences = {
      shake: 1,
      detail: 1,
      sound: 1,
      music: 1,
      volume: 100,
      zoom: 100,
      use_ruffle: 1
    }
  }
  const adv = await models.Adventure.getBySlug(req.session, req.params.adventure)
  if (adv === null) {
    return res.status(404).render('404')
  }
  const levels = {}
  Object.keys(adv.adventure.levels.sets).reverse().forEach(k => levels[k] = adv.adventure.levels.sets[k])
  res.render(req.params.mode !== 'editor' ? 'adventures/play' : 'adventures/editor',
             {adventure: adv,
              sets: qs.stringify(levels)})
})

module.exports = router
