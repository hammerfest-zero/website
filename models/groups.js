const groups = {}

groups.getById = async function(db, id) {
  const query = '\
SELECT `groups`.`id`, `groups`.`name`, \
  JSON_OBJECT("id", `owner`.`id`, "name", `owner`.`name`, "display_name", `owner_preferences`.`display_name`) AS `owner`, \
  CONCAT("[", GROUP_CONCAT(JSON_OBJECT("id", `users`.`id`, "name", `users`.`name`, "display_name", `user_preferences`.`display_name`)), "]") AS `users` \
FROM `groups` \
LEFT JOIN `users` AS `owner` ON `owner`.`id` = `groups`.`owner_id` \
LEFT JOIN `user_preferences` AS `owner_preferences` ON `owner_preferences`.`user_id` = `owner`.`id` \
LEFT JOIN `users_groups` ON `users_groups`.`group_id` = `groups`.`id` \
LEFT JOIN `users` ON `users`.`id` = `users_groups`.`user_id` \
LEFT JOIN `user_preferences` ON `user_preferences`.`user_id` = `users`.`id` \
WHERE `groups`.`id` = ?'
  const rows = await db.queryAsync(query, [id])
  rows.forEach(r => {
    r.users = JSON.parse(r.users)
    r.owner = JSON.parse(r.owner)
  })
  return rows
}

groups.create = async function(db, params) {
  if (params.name[0] === '_') {
    throw new Error('A group name can not start with an underscore `_`')
  }
  const query = 'START TRANSACTION; \
SET @user_id = ?; \
INSERT INTO `groups` (`owner_id`, `name`) VALUES(@user_id, ?); \
SET @group_id = LAST_INSERT_ID(); \
INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES(@user_id, @group_id); \
COMMIT'
  try {
    const rows = await db.queryAsync(query, [params.user_id, params.name])
  } catch(e) {
    await db.queryAsync('ROLLBACK')
    if (e.code === 'ER_DUP_ENTRY') {
      throw new Error('Another group with this name already exists.')
    }
  }
  return rows
}

groups.deleteById = async function(db, id) {
  return await db.queryAsync('DELETE FROM `groups` WHERE `id` = ?', [id])
}

module.exports = groups
