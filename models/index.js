const util = require('util')

module.exports = function (db) {
  db.queryAsync = util.promisify(db.query).bind(db)

  const models = {}

  models.User = require('./users')
  models.Group = require('./groups')
  models.Adventure = require('./adventures')

  for (const [_, model] of Object.entries(models)) {
    for (const [name, method] of Object.entries(model)) {
      model[name] = (...args) => method(db, ...args)
    }
  }

  return models
}
