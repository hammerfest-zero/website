const argon2 = require('argon2')

const users = {}

async function doGetQuery(db, discriminator, value) {
  const query ='\SELECT `users`.`id`, `users`.`name`, `users`.`password`, `users`.`email`, \
`user_preferences`.`display_name`, `user_preferences`.`profile_private`, `user_preferences`.`game_preferences`, \
CONCAT( \
  "[", \
  IF(`groups`.`id`, \
     GROUP_CONCAT(JSON_OBJECT("id", `groups`.`id`, "name", `groups`.`name`, "is_owner", `users`.`id` = `groups`.`owner_id`)), \
     ""), \
  "]") AS "groups" \
FROM `users` \
LEFT JOIN `user_preferences` ON `user_preferences`.`user_id` = `users`.`id` \
LEFT JOIN `users_groups` ON `users_groups`.`user_id` = `users`.`id` \
LEFT JOIN `groups` ON `users_groups`.`group_id` = `groups`.`id` \
WHERE ' + discriminator + ' = ? \
GROUP BY `users_groups`.`user_id`'
  const rows = await db.queryAsync(query, [value])
  rows.forEach(r => {
    r.profile_private = Boolean(r.profile_private)
    r.groups = JSON.parse(r.groups)
    r.game_preferences = JSON.parse(r.game_preferences)
  })
  return rows
}

users.getById = async function(db, id) {
  return await doGetQuery(db, '`users`.`id`', id)
}

users.getByName = async function(db, name) {
  return await doGetQuery(db, '`users`.`name`', name)
}

users.create = async function(db, params) {
  const queryParams = [
    params.username,
    await argon2.hash(params.password),
    null,
    params.display_name || params.username,
    params.profile_private !== undefined ? Boolean(params.profile_private) : true,
    JSON.stringify({
      "sound": true,
      "music": true,
      "detail": true,
      "shake": true,
      "volume": 100,
      "zoom": 100,
      "use_ruffle": true
    })
  ]
  const query = 'START TRANSACTION; \
SET @user_name = ?; \
INSERT INTO `users` (`name`, `password`, `email`) VALUES(@user_name, ?, ?); \
SET @user_id = LAST_INSERT_ID(); \
INSERT INTO `user_preferences` (`user_id`, `display_name`, `profile_private`, `game_preferences`) VALUES(@user_id, ?, ?, ?); \
INSERT INTO `groups` (`owner_id`, `name`) VALUES(@user_id, CONCAT("_", @user_name)); \
SET @group_id = LAST_INSERT_ID(); \
INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES(@user_id, @group_id); \
COMMIT'
  try {
    return await db.queryAsync(query, queryParams)
  } catch(e) {
    await db.queryAsync('ROLLBACK')
    if (e.code === 'ER_DUP_ENTRY' && e.sqlMessage.slice(-5, -1) === 'name') {
      throw new Error('Username already in use.')
    } else if (e.code === 'ER_DUP_ENTRY' && e.sqlMessage.slice(-6, -1) === 'email') {
      throw new Error('Email address already exists.')
    } else {
      throw e
    }
  }
}

users.updateById = async function(db, user_id, params) {
  const queryParams = [
    params.password ? await argon2.hash(params.password) : null,
    params.display_name,
    params.profile_private !== undefined ? Boolean(params.profile_private) : true,
    JSON.stringify(params.game_preferences),
    user_id
  ]
  const query = 'UPDATE `users` \
LEFT JOIN `user_preferences` ON `users`.`id` = `user_preferences`.`user_id` \
SET `users`.`password` = COALESCE(?, `users`.`password`), \
    `user_preferences`.`display_name` = COALESCE(?, `user_preferences`.`display_name`), \
    `user_preferences`.`profile_private` = COALESCE(?, `user_preferences`.`profile_private`), \
    `user_preferences`.`game_preferences` = COALESCE(?, `user_preferences`.`game_preferences`) \
WHERE `users`.`id` = ?'
  return await db.queryAsync(query, queryParams)
}

users.deleteById = async function(db, id) {
  return await db.queryAsync('DELETE FROM `users` WHERE `id` = ?', [id])
}

module.exports = users
