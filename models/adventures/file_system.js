const fs = require('fs').promises
const path = require('path')
const Adventure = require('adventure')

const advDir = path.join(__dirname, '..', '..', 'public', 'adventures')

const adventures = {}

async function getByFile(filename) {
  const name = filename.split(".").slice(0, -1).join(".")
  return {
    source: "disk",
    slug: "f" + name,
    adventure: await Adventure.fromFile(path.join(advDir, filename)),
    group: null,
    user: null,
    permisions: {
      group_read: true,
      group_write: false,
      group_fork: true,
      other_read: true,
      other_fork: true
    }
  }
}

adventures.getAll = async function() {
  const advs = []
  const files = await fs.readdir(advDir)
  await Promise.all(files.map(async f => {
    try {
      advs.push(await getByFile(f))
    } catch (e) {}
  }))
  return advs
}

adventures.getByName = async function(name) {
  try {
    return await getByFile(name + ".json")
  } catch (e) {
    return null
  }
}

module.exports = adventures
