const crypto = require('crypto')
const util = require('util')
const Adventure = require('adventure')

const randomBytes = util.promisify(crypto.randomBytes)

const adventures = {}

function getByName(session = {}, name) {
  if ((session.advs || {})[name] == null) {
    return null
  }
  return {
    source: "session",
    slug: "s" + name,
    adventure: Adventure.fromJSON((session.advs || {})[name]),
    group: null,
    user: null,
    permisions: {
      group_read: true,
      group_write: false,
      group_fork: true,
      other_read: true,
      other_fork: true
    }
  }
}

adventures.getAll = function(session = {}) {
  return (Object.entries(session.advs || {}))
    .map(([name, _]) => getByName(session, name))
}

adventures.getByName = getByName

adventures.create = async function(session = {}, adventure) {
  if (!session.advs) {
    session.advs = {}
  }
  const md5sum = crypto
        .createHash("md5")
        .update(JSON.stringify(adventure) + await (randomBytes(16).toString("hex")))
        .digest("hex")
  session.advs[md5sum] = adventure.toJSON()
  return md5sum
}

adventures.deleteByName = function(session = {}, name) {
  if (session.advs && session.advs[name]) {
    delete session.advs[name]
    return true
  } else {
    return false
  }
}

module.exports = adventures
