const Adventure = require('adventure')

const adventures = {}

adventures.create = async function(db, params) {
  const queryParams = [
    params.user_id,
    params.group_id,
    params.version ?? null,
    params.body
  ]
  const query = 'START TRANSACTION; \
INSERT INTO `permissions` () VALUES (); \
SET @permission_id = LAST_INSERT_ID(); \
INSERT INTO `adventures` (`user_id`, `group_id`, `permissions_id`, `version`, `body`) VALUES(?, ?, @permission_id, COALESCE(?, version), ?); \
COMMIT'
  try {
    return (await db.queryAsync(query, queryParams))[3].insertId
  } catch(e) {
    await db.queryAsync('ROLLBACK')
    throw e
  }
}

const getQuery = '\
SET @user_id = ?; \
SELECT `adventures`.`id`, \
       "db" AS `source`, \
       `adventures`.`body` AS `adventure`, \
       JSON_OBJECT("id", `users`.`id`, \
                   "name", `users`.`name`, \
                   "display_name", `user_preferences`.`display_name`, \
                   "private", `user_preferences`.`profile_private`) AS `user`, \
       JSON_OBJECT("id", `groups`.`id`, \
                   "name", `groups`.`name`) AS `group`, \
       JSON_OBJECT("group_read", `permissions`.`group_read`, \
                   "group_write", `permissions`.`group_write`, \
                   "group_fork", `permissions`.`group_fork`, \
                   "other_read", `permissions`.`other_read`, \
                   "other_fork", `permissions`.`other_fork`) AS `permissions` \
FROM `adventures` \
LEFT JOIN `users` ON `users`.`id` = `adventures`.`user_id` \
LEFT JOIN `user_preferences` ON `users`.`id` = `user_preferences`.`user_id` \
LEFT JOIN `groups` ON `groups`.`id` = `adventures`.`group_id` \
LEFT JOIN `permissions` ON `permissions`.`id` = `adventures`.`permissions_id` \
WHERE (`users`.`id` = @user_id \
       OR `permissions`.`other_read` \
       OR (`permissions`.`group_read` AND (SELECT COUNT(*) FROM `users_groups` WHERE `users_groups`.`user_id` = @user_id AND `users_groups`.`group_id` = `group_id`)))'

function rowToAdventure({adventure, source, id, group, permissions, user}, user_id) {
  user = JSON.parse(user)
  user.private = Boolean(user.private)
  return {
    id,
    source,
    adventure: Adventure.fromJSON(JSON.parse(adventure)),
    slug: "d" + id,
    goup: JSON.parse(group),
    permissions: Object.fromEntries(
      Object.entries(JSON.parse(permissions)).map(([k, v]) => [k, Boolean(v)])
    ),
    user: user.private && user.id != user_id ? null : user
  }
}

adventures.getAll = async function(db, user_id) {
  const query = getQuery
  try {
    const rows = (await db.queryAsync(query, [user_id]))[1]
    return rows.map(r => rowToAdventure(r, user_id))
  } catch(e) {
    throw e
  }
}

adventures.getByAuthor = async function(db, user_id, author_id) {
  const query = getQuery + " AND `adventures`.`user_id` = ?"
  try {
    const rows = (await db.queryAsync(query, [user_id, author_id]))[1]
    return rows.map(r => rowToAdventure(r, user_id))
  } catch(e) {
    throw e
  }
}

adventures.getById = async function(db, user_id, adventure_id) {
  const query = getQuery + " AND `adventures`.`id` = ?"
  try {
    const row = (await db.queryAsync(query, [user_id, adventure_id]))[1][0]
    return row ? rowToAdventure(row, user_id) : null
  } catch(e) {
    throw e
  }
}

adventures.deleteById = async function(db, user_id, adventure_id) {
  const query = '\
DELETE `adventures`, `permissions` \
FROM `adventures` \
INNER JOIN `permissions` ON `permissions`.`id` = `adventures`.`permissions_id` \
WHERE `adventures`.`user_id` = ? \
      AND `adventures`.`id` = ?'
  try {
    const row = (await db.queryAsync(query, [user_id, adventure_id]))
    return row.affectedRows === 2
  } catch(e) {
    throw e
  }
}

module.exports = adventures
