const sources = {
  fileSystem: require('./file_system'),
  session: require('./session'),
  db: require('./db')
}

const adventures = {}

adventures.getAll = async function(db, session) {
  return [...sources.session.getAll(session),
          ...(await sources.fileSystem.getAll()),
          ...(await sources.db.getAll(db, session?.user?.id))]
}

adventures.getBySlug = async function(db, session, slug) {
  switch (slug[0]) {
  case 'd':
    return await sources.db.getById(db, session?.user?.id, parseInt(slug.slice(1)))
  case 'f':
    return await sources.fileSystem.getByName(slug.slice(1))
  case 's':
    return sources.session.getByName(session, slug.slice(1))
  default:
    return null
  }
}

adventures.getByAuthor = async function(db, session, author_id) {
  return await sources.db.getByAuthor(db, session?.user?.id, author_id)
}

adventures.getAllFromSession = async function(db, session) {
  return sources.session.getAll(session)
}

adventures.create = async function(db, session, adventure) {
  if (session?.is_logged === true) {
    return "d" + await sources.db.create(db, {
      user_id: session.user.id,
      group_id: session.user.groups[0].id,
      body: JSON.stringify(adventure)
    })
  } else {
    return "s" + await sources.session.create(session, adventure)
  }
}

adventures.deleteBySlug = async function(db, session, slug) {
  switch (slug[0]) {
  case 'd':
    return await sources.db.deleteById(db, session?.user?.id, parseInt(slug.slice(1)))
  case 's':
    return sources.session.deleteByName(session, slug.slice(1))
  default:
    return false
  }
}

adventures.sessionToDb = async function(db, session) {
  //TODO: parallelize this
  for (const [name, adv] of Object.entries(session.advs || {})) {
    try {
      await sources.db.create(db, {
        user_id: session.user.id,
        group_id: session.user.groups[0].id,
        body: JSON.stringify(adv)
      })
      delete session.advs[name]
    } catch(e) {
      //TODO: return warning/error
    }
  }
}

module.exports = adventures
