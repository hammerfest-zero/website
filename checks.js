exports.needAuth = function(req, res, next) {
  if (req.session.is_logged === true) {
    next()
  } else {
    /* TODO: use flash message instead */ 
    res.locals.form.errors.unknown = "You need to be logged in to access this page."
    res.status(401).render('login')
  }
}

exports.needNoAuth = function(req, res, next) {
  if (req.session.is_logged === true) {
    /* TODO: add a flash message */
    res.redirect(303, '/')
  } else {
    next()
  }
}
