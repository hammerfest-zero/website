const Adventure = require('adventure')

class Editor {
  constructor(domElement) {
    this.game = document[domElement]
    this.files = {}
    this.isModified = false
  }

  setModified(val) {
    this.idModified = val
  }

  setFile(name, val) {
    this.files[name] = val
  }

  askCurrentFile() {
    this.game.askCurrentFile()
  }
}

window.addEventListener('load', () => {
  window.editor = new Editor('game')
  window.adventure = Adventure.fromJSON(adventureJSON || {})

  window.fields = {
    "name": document.getElementById('name'),
    "description": document.getElementById('desc'),
    "lives": document.getElementById('lives'),
    "bombs": document.getElementById('bombs')
  }

  window.fields.name.value = adventure.name.toString()
  window.fields.description.value = adventure.description.toString()
  window.fields.lives.value = adventure.game.lives
  window.fields.bombs.value = adventure.game.bombs

  window.fields.name.addEventListener('change', e => {
    adventure.name.default = e.target.value
  })

  window.fields.description.addEventListener('change', e => {
    adventure.description.default = e.target.value
  })

  window.fields.lives.addEventListener('change', e => {
    adventure.game.lives = parseInt(e.target.value)
  })

  window.fields.bombs.addEventListener('change', e => {
    adventure.game.bombs = parseInt(e.target.value)
  })

  Object.entries(window.fields).forEach(([_, field]) => {
    field.addEventListener('change', updateDownloadButton)
  })

  updateDownloadButton()
})

/* Functions used to interact with SWF element */
window.setEditorModifiedFlag = function(value) {
  editor.setModified(value)
  return true
}

window.setEditorFile = function(name, val) {
  editor.setFile(name, val)
  adventure.levels.sets[name] = val

  updateDownloadButton()
  return true
}

window.updateDownloadButton = function() {
  const button = document.getElementById('downloadButton')
  button.setAttribute('href', 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(adventure.toJSON())))
  button.setAttribute('download', (adventure.name.toString() || "adventure") + ".adv.json")
}
